/***************************************************************************
                          allele.cpp  -  description
                             -------------------
    begin                : Thu Sep 14 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


//#include "entetepop.h"
#include "allele.h"
#include "locus.h"

// constructeur
Allele::Allele(Locus *Ploc)
{
  _nom = "";
  //	strcpy (_nomgpop,_nom.c_str());
  _Ploc = Ploc;

  _miss = r_ismissingvalue(_nom);
  //	if ((_nom == "00") || (_nom == "NULL") || (_nom == "NULL") || (_nom ==
  //"NULL")) _nul = true; 	else _nul = false;
}


Allele::Allele(const Allele &original, Locus *Ploc)
{
  // constructeur de copies
  _nom = original._nom;

  _Ploc    = Ploc;
  _miss    = original._miss;
  _nbrepet = original._nbrepet;
}


Allele::Allele(Locus *Ploc, const string &nom)
{
  _nom = nom.c_str();
  //	strcpy (_nomgpop,_nom.c_str());
  _Ploc    = Ploc;
  _nbrepet = atoi(nom.c_str());

  _miss = r_ismissingvalue(_nom);
}

Allele::Allele(Locus *Ploc, const char *nom)
{
  _nom = nom;
  //	strcpy (_nomgpop,_nom.c_str());
  _Ploc    = Ploc;
  _nbrepet = atoi(nom);

  _miss = r_ismissingvalue(nom);
}

// destructeur
Allele::~Allele()
{
  //	delete [] _tabPind;
}

bool
Allele::f_verifnum(int typedenum) const
{
  // Vérification du type de numérotation des allèles
  //	1 => type Genepop stricte (2digits)
  //	2 => nom d'alleles = numéros
  //	3 => nom d'alleles = numéros < 999
  // Anomalie 2-> les numéros d'allèles format genepop ne sont pas présents

  switch(typedenum)
    {
      case 1:
        if(_nom.size() != 2)
          return (false);
        if((_nom[0] < 48) || ((_nom[0] > 57)))
          return (false);
        if((_nom[1] < 48) || ((_nom[1] > 57)))
          return (false);

        break;
      case 2:
        for(int i = 0; i < _nom.size(); i++)
          {
            if((_nom[i] < 48) || ((_nom[i] > 57)))
              return (false);
          }

        break;

      case 3:
        if(_nom.size() > 3)
          return (false);
        for(int i = 0; i < _nom.size(); i++)
          {
            if((_nom[i] < 48) || ((_nom[i] > 57)))
              return (false);
          }

        break;
    }

  return (true);
}

const Allele &
Allele::operator=(const Allele &rval)
{

  _nom     = rval._nom;
  _miss    = rval._miss;
  _nbrepet = rval._nbrepet;
  return (*this);
}

bool
Allele::operator==(const Allele &rval) const
{
  // egalité basée sur les noms des allèles et des locus correspondants
  //   PAS sur les pointeurs !!!
  if(rval._Ploc->get_nom() != _Ploc->get_nom())
    return (false);

  return ((_miss && rval._miss) || (rval._nom == _nom));
}


const string
Allele::get_NomLocus() const
{
  return (_Ploc->get_nom());
}

bool
Allele::r_ismissingvalue(const QString &nom) const
{
  if((nom == "0") || (nom == "000") || (nom == "00") || (nom == "null") ||
     (nom == "NULL") || (nom == "miss") || (nom == "missing"))
    return true;
  else
    return false;
}

bool
Allele::checkAllele() const
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " this=" << this;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " _nom=" << _nom;

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " get_NomLocus=" << get_NomLocus().c_str();
  return true;
}
