/***************************************************************************
                          arbreplus.h  -  description
                             -------------------
    begin                : Mon Jun 4 2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ARBREPLUS_H
#define ARBREPLUS_H

#include "arbre.h"
#include "couleur.h"

namespace biolib {
namespace arbres {

/**Arbre + informations sur les groupes d'OTUs
  *@author Olivier Langella
  */
class ArbrePlus;

class ArbrePlGroupes : public vector<ArbreVectUINT> {
// ArbreGroupe contient des groupes d'individus valables pour 1 ArbrePlus
public:
	ArbrePlGroupes(const ArbrePlus * Parbre);
	~ArbrePlGroupes();

	const Couleur & get_couleur(unsigned int i) const {return (_tabCouleur[i]);};
	const string & get_nom(unsigned int i) const {return (_tabNom[i]);};

	void push_back_groupe(const string & nom, const string & couleur);

	int RechercheGroupeOTU(unsigned int id) const;

protected:

	const ArbrePlus * _Parbre;

	vector<Couleur> _tabCouleur;
	vector<string> _tabNom;
//	vector<string> _tabReference;
	
};

class ArbrePlus : public Arbre  {
public: 
	ArbrePlus();
	~ArbrePlus();

	const Couleur & get_couleur_neutre () const {return(_couleur_neutre);};
	const Couleur & get_couleur_otu (unsigned int id) const;

	void define_outgroup(const ChaineCar & outgroup);
	void ajouter_groupe(const ChaineCar & groupe, const string & couleur, const string & nom="");

protected:

	virtual void iPhylipDefGroupe(const string &); //définition d'un groupe
	virtual void oPhylipEcritGroupes(ostream &) const;


	ArbrePlGroupes _tabGroupes;

	ArbreVectUINT _Vind_outgroup;

	Couleur _couleur_neutre;	
};

} //namespace biolib {
} //namespace arbres {


#endif
