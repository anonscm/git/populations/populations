//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

//created:
//mer sep 06 14:23:39 CEST 2000 Langella <Olivier.Langella@pge.cnrs-gif.fr>


/***************************************************************************
                          chaineficpop.cpp  -  Objet pour faciliter la lecture de fichiers genepop/populations
                             -------------------
    begin                : ven sep 06 10:25:55 CEST 2000
    copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

#include "chaineficpop.h"

//constructeur
ChaineFicPop::ChaineFicPop(istream & fichier):ChaineCar(""), _fichier(fichier) {
	//_fichier(fichier);
	_fichier.clear();
	_fichier.seekg(0);

	_numeroligne = 0;
	_type_fichier = 0;
	_danspop = false;
	_findefichier = false;
	_boolnompop =  false;
}

//destructeur
ChaineFicPop::~ChaineFicPop() {
	_fichier.clear();
}

int ChaineFicPop::get_titre(biolib::vecteurs::Titre & titres) {
	//lecture des commentaires
//cerr << " ChaineFicPop::get_titre debut" << endl;
//  assign("");
	titres.resize(0);

	if (estuntitre()) titres.push_back(*this);
	
//cerr << " ChaineFicPop::get_titre debut 1" << endl;
	while ((get_ligne()) && (estuntitre())) {
		titres.push_back(*this);
		assign("");
	};
	
//cerr << " ChaineFicPop::get_titre debut 2" << endl;
	if (_fichier.fail()) throw Anomalie(1);

//cerr << " ChaineFicPop::get_titre debut 3" << endl;
	return (0);
}

int ChaineFicPop::get_locus(biolib::vecteurs::Titre & locus) {
	//lecture des locus

	locus.resize(0);

	if ((estunepop()==false) && (*this != "")) ajout_locus(locus);
	
	while ((get_ligne()) && (estunepop()==false)) {
		ajout_locus(locus);
	};
	
	if (_fichier.fail()) throw Anomalie(1);
	if (estunepop()==false) throw Anomalie(2);

	return (0);

}

int ChaineFicPop::get_lignepop() {
	if (_findefichier) return(1);
	get_ligne();
	return(0);
}

int ChaineFicPop::get_nompop(biolib::vecteurs::ChaineCar & nompop) const {

	nompop.assign(_nompop);
//	nompop.Remplacer(" ","_");
/*	if ((GetNbMots()) > 1) GetMot(2, nompop);
	else nompop.assign("");
*/
	return(0);
}

int ChaineFicPop::get_nomind(biolib::vecteurs::ChaineCar & nomind) const {
	if (!(_danspop)) return (1);
	int pos;
	
	pos = find(",", 0);
	if (pos != -1) {
//		ChaineCar mot;
		
		nomind.assign(*this,0, pos);
		nomind.fsupprgauche();
		nomind.fsupprdroite();
//		nomind.assign(mot);
	}
//	nomind.Remplacer(" ","_");

	return(0);
	
}

int ChaineFicPop::get_alleles(biolib::vecteurs::Titre & alleles) const {
//cerr << " ChaineFicPop::get_alleles" << endl;
	if (!(_danspop)) return (1);
	alleles.resize(0);
	int pos;
	
	ChaineCar mots;
	string mot;
	long nbmots, i;
	
	pos = find(",", 0) + 1;
	if (pos != -1) {
		mots.assign(*this,pos, size() - pos);
		
		nbmots = mots.GetNbMots();

		for (i=0; i < nbmots; i++) {
			mots.GetMot(i+1, mot);
			if (mot != "") alleles.push_back(mot);
		}	
	}
	else  return (1);

//cerr << " ChaineFicPop::get_alleles fin 0" << endl;
	return(0);
}

long ChaineFicPop::get_numeroligne() const{
	return(_numeroligne);
}

bool ChaineFicPop::get_ligne() {
//cerr << " ChaineFicPop::get_ligne debut" << endl;
	GetLigneFlot(_fichier);
	_numeroligne++;

	if (_fichier.eof()) _findefichier = true;
	
//cerr << " ChaineFicPop::get_ligne fin" << endl;
	return (_fichier.good());
}

bool ChaineFicPop::estuntitre() {
// détecte les signes d'un titre: """, "Title Line:" ...
	string titre;

  if (size() < 1) return (false);
//cerr << " ChaineFicPop::estuntitre debut" << endl;
	if (operator[](0) == '"') {
//		if (operator[](size()-1) == '"') string titre(*this,1, size() -2);
//cerr << " ChaineFicPop::estuntitre titre " << (size() - 1) << endl;
		titre.assign(*this,1, size() -1);
//cerr << " ChaineFicPop::estuntitre titre " << titre << endl;
		assign(titre);
		return (true);
	}
//cerr << " ChaineFicPop::estuntitre debut 2" << endl;
	int pos;
	string mot("Title line: ");
	
//cerr << " ChaineFicPop::estuntitre debut 3" << endl;
	pos = find(mot, 0);
	if (pos != -1) {
		titre.assign(*this,pos + mot.size(), size() - mot.size() - pos);
		assign(titre);
		return (true);
	}
	
//cerr << " ChaineFicPop::estuntitre debut 4" << endl;
	if (_numeroligne == 1) return(true);

	return (false);
}

bool ChaineFicPop::estunepop() {
// détecte les signes d'un titre: """, "Title Line:" ...
//	string _chainetemp;
	int pos;

	GetMot(1, _chainetemp);
	if ((_chainetemp == "POP")||(_chainetemp == "pop")||(_chainetemp == "Pop")) {
		pos = find(",", 0);
		if (pos > 0) return (false);

		_danspop = true;
		if (size() > 4) {
			_nompop = substr(4, (size() - 4));
			_boolnompop = true; //les noms de pop sont derrière le mot clé pop
		}
		else _nompop.assign("");
		
		return (true);
	}
	return (false);

}

bool ChaineFicPop::estunindividu() const {
// détecte les signes d'un titre: """, "Title Line:" ...
	if (!(_danspop)) return (false);
	
//	string mot;
	
	if (GetNbMots(",") == 2) return (true);
	else return (false);
/*	GetMot(1, mot);
	if (mot[mot.size()-1] == ',') return (true);
	else {
		GetMot(2, mot);
		if (mot == ",") return true;
	}
	return (false);   */
}

void ChaineFicPop::ajout_locus(biolib::vecteurs::Titre & locus) const {
	long nbmots, i;
	string mot;
	
	nbmots = GetNbMots();

	for (i=0; i < nbmots; i++) {
		GetMot(i+1, mot);
		if (mot != "") locus.push_back(mot);
	}

}

bool ChaineFicPop::findefichier() const {
	return (_findefichier);
}

bool ChaineFicPop::get_boolnompop() const {
	return (_boolnompop);
}
