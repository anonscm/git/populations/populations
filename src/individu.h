/***************************************************************************
                          individu.h  -  description
                             -------------------
    begin                : Thu Sep 14 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// individu
#pragma once

#include "allele.h"

using namespace biolib::vecteurs;

class Individu
{
  public:
  //
  Individu(Population *);
  Individu(Population *Ppop, const string &nom);
  Individu(Population *, const ConstGnt &, int);

  Individu(const Individu &other);

  Individu(const Individu &, Population *);

  //	Individu (int nball=0);
  ~Individu();

  bool checkIndividu() const;
  bool checkAlleleArraySize() const;

  const Individu &operator=(const Individu &);
  bool operator==(const Individu &) const;
  bool r_estnul() const;
  bool r_esthetero(unsigned long locus) const;
  unsigned long r_nbcopall(Allele *Pall) const;
  unsigned long r_nballnonnuls(unsigned long locus,
                               unsigned int nploidie) const;

  unsigned long get_nballnuls() const;
  unsigned long get_nballnuls(unsigned long) const;
  void set_Pallele(Allele *Pall, unsigned int pos);

  inline const string &
  get_nom() const
  {
    return (_nom);
  };
  unsigned int
  get_nploidie() const
  {
    return (_nploidie);
  };
  const vector<Allele *> &get_tabPall() const;
  const string get_nom_all(unsigned long numlocus, unsigned long numall) const;

  void resize_alleles();

  void affiche(ostream &, int) const;

  friend class Population;
  friend class StrucPop;
  friend class DistancesGnt;
  friend class Allele;
  friend class Jeupop;

  void ifFusionnerIndividu(const Individu &individu);
  void ifPlacerAllele(Locus *Ploc, long position, const Allele *Pall);

  void oPopulationsXML(unsigned int id, ostream &sortie, ostream &infos) const;

  const Allele *getAllelePtr(std::size_t offset) const;
  const Allele *getAllelePtr(unsigned long numlocus, unsigned long numall) const;
  
  void push_back_AllelePtr(Allele *Pall);

  private:
  void creation(Population *Ppop);
  /** Suppresssion d'un locus */
  void supprLocus(unsigned int);

  string _nom;
  //	char _nom[20];
  Population *m_Ppop = nullptr;
  //	unsigned long _nball; //nb allèles= nb de loci* n ploidie
  unsigned int _nploidie;
  std::vector<Allele *> m_tabPall;

  public:
  struct Anomalie
  {
    // 1-> copie impossible: pas le meme nombre d'alleles
    // 2-> no population pointer
    // 11-> numero de locus inexistant
    // 12-> numéro d'allele inexistant
    // 13-> un allele nul fausse la determination de l'heterozygotie de
    // l'individu pour un locus
    // 14-> allele pointer array size is not consistent
    // 15-> allele pointer == nullptr
    int le_pb;
    Anomalie(int i) : le_pb(i){};
  };
};

