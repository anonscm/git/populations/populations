/***************************************************************************
                          individu.cpp  -  description
                             -------------------
    begin                : Thu Sep 14 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "individu.h"
#include "population.h"
#include "jeupop.h"


void
Individu::creation(Population *Ppop)
{
  if(Ppop == nullptr)
    {
      throw Anomalie(2);
    }
  m_Ppop    = Ppop;
  _nploidie = m_Ppop->get_nploidie();
  if(m_Ppop->get_nploidie() == 0)
    _nploidie = 0;
  else
    _nploidie = m_Ppop->get_nploidie();
  m_tabPall.clear();
  m_tabPall.resize(m_Ppop->get_nbloc() * _nploidie, nullptr);
}

// constructeur
Individu::Individu(Population *Ppop)
{
  creation(Ppop);
  _nom.assign("");
}

Individu::Individu(const Individu &other)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  creation(other.m_Ppop);
  _nom = other._nom;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

Individu::Individu(Population *Ppop, const string &nom)
{

  creation(Ppop);
  _nom.assign(nom);
}

// destructeur
Individu::~Individu()
{
  // cerr << "Individu::~Individu ()" << endl;
  //	delete [] _tabPall;
}

void
Individu::resize_alleles()
{

  _nploidie = m_Ppop->get_nploidie();
  m_tabPall.resize(m_Ppop->get_nbloc() * _nploidie, nullptr);
}

const Individu &
Individu::operator=(const Individu &rval)
{
  unsigned long i; // index
  unsigned long j; // locus
  string nomlocus;
  unsigned long nball(rval.m_tabPall.size());
  unsigned long nploidie(m_Ppop->get_nploidie());
  unsigned long nbloc(m_Ppop->get_nbloc());

  if(m_tabPall.size() != nball)
    throw Anomalie(1);


  _nom = rval._nom;

  // cerr << "operator= individu debut " << _nom << endl;
  for(j = 0; j < nbloc; j++)
    {
      nomlocus = rval.m_Ppop->_Pjeu->get_nomlocus(j);
      for(i = 0; i < nploidie; i++)
        {
          // cerr << "operator= individu numall " << (j*nploidie) + i << endl;
          if(rval.m_tabPall[(j * nploidie) + i] == nullptr)
            {
              m_tabPall[(j * nploidie) + i] =
                (m_Ppop->_Pjeu->get_Plocus(nomlocus))->getPallNul();
            }
          else
            {
              // cerr << "operator= individu nom de l'allele " <<
              // rval._tabPall[(j * nploidie) + i]->_nom << endl;
              m_tabPall[(j * nploidie) + i] = m_Ppop->get_Pall(
                nomlocus, rval.m_tabPall[(j * nploidie) + i]->get_nom());
            }
        }
    }
  // cerr << "operator= individu fin" << endl;

  return (*this);
}

bool
Individu::r_estnul() const
{
  long i; // index
  long nball(m_tabPall.size());

  for(i = 0; i < nball; i++)
    if(m_tabPall[i]->_miss == false)
      return (false);
  return (true);
}

bool
Individu::r_esthetero(unsigned long locus) const
{
  // lancer une exception si l'individu n'est pas diploide !!!!

  unsigned long nploidie(m_Ppop->get_nploidie());
  unsigned long nbloc(m_Ppop->get_nbloc());

  if(locus >= nbloc)
    throw Anomalie(11);
  if(nploidie != 2)
    throw Anomalie(12);

  if((m_tabPall[(locus * 2)]->_miss) || (m_tabPall[(locus * 2) + 1]->_miss))
    {
      throw Anomalie(13);
      return (false);
    }
  if(m_tabPall[(locus * 2)] == m_tabPall[(locus * 2) + 1])
    return (false);
  else
    return (true);
}

bool
Individu::operator==(const Individu &rval) const
{

  // pas operationnel !!!!

  unsigned long i; // index
  unsigned long j; // locus
  string nomlocus;
  unsigned long nball(rval.m_tabPall.size());
  unsigned long nploidie(m_Ppop->get_nploidie());
  unsigned long nbloc(m_Ppop->get_nbloc());

  //	cerr << "truc";
  if(rval == 0)
    {
      //		cerr << "truc";
      nball = m_tabPall.size();
      for(i = 0; i < nball; i++)
        if(m_tabPall[i]->_miss == false)
          return (false);
      return (true);
    }

  if(m_tabPall.size() != nball)
    return (false);


  //	_nom = rval._nom;

  for(j = 0; j < nbloc; j++)
    {
      //		nomlocus = rval._Ppop->_Pjeu->get_nomlocus(j);
      for(i = 0; i < nploidie; i++)
        {
          if(m_tabPall[(j * nploidie) + i] !=
             rval.m_tabPall[(j * nploidie) + i])
            return (false);
          //			_tabPall[(j*nploidie) + i] = _Ppop->get_Pall(nomlocus,
          // rval._tabPall[(j*nploidie) + i]->_nom);
        }
    }

  return (true);
}


Individu::Individu(const Individu &rval, Population *Ppop)
{

  if(Ppop == nullptr)
    {
      throw Anomalie(2);
    }
  unsigned long i; // index
  unsigned long j; // locus
  string nomlocus;
  unsigned long nball(rval.m_tabPall.size());
  // cerr << "Individu::Individu construct copie début "<< nball << endl;
  unsigned long nploidie(Ppop->get_nploidie());
  // cerr << "Individu::Individu construct copie début "<< nball << endl;
  unsigned long nbloc(Ppop->get_nbloc());

  // cerr << "Individu::Individu construct copie début " << endl;
  m_Ppop = Ppop;
  resize_alleles();
  if(m_tabPall.size() != nball)
    throw Anomalie(1);

  _nom = rval._nom;

  // cerr << "operator= individu debut " << _nom << endl;
  for(j = 0; j < nbloc; j++)
    {
      nomlocus = rval.m_Ppop->_Pjeu->get_nomlocus(j);
      for(i = 0; i < nploidie; i++)
        {
          // cerr << "operator= individu numall " << (j*nploidie) + i << endl;
          if(rval.m_tabPall[(j * nploidie) + i] == nullptr)
            {
              m_tabPall[(j * nploidie) + i] =
                (m_Ppop->_Pjeu->get_Plocus(nomlocus))->getPallNul();
            }
          else
            {
              // cerr << "operator= individu nom de l'allele " <<
              // rval._tabPall[(j * nploidie) + i]->_nom << endl;
              m_tabPall[(j * nploidie) + i] = m_Ppop->get_Pall(
                nomlocus, rval.m_tabPall[(j * nploidie) + i]->get_nom());
            }
        }
    }
  // cerr << "operator= individu fin" << endl;
  // cerr << "Individu::Individu construct copie fin " << endl;
}


void
Individu::ifPlacerAllele(Locus *Ploc, long position, const Allele *Pall)
{
  // numall < nploidie
  //	long position((Ploc->get_numloc()
  // cerr << "coucou 3" << endl;
  if((m_tabPall[position] == nullptr) || (m_tabPall[position]->_miss))
    {
      // on remplace, normal
      // cerr << "coucou 3 if" << endl;
      m_tabPall[position] = Ploc->getPall(Pall->get_nom());
    }
  else
    {
      if(*m_tabPall[position] == *Pall)
        {
          // c'est bon
          return;
        }
      else
        {
          // ca veut dire qu'il y a un allele, et qu'il est different
          //   => pb, mais on ignore pour l'instant
          return;
        }
    }
}

void
Individu::ifFusionnerIndividu(const Individu &individu)
{
  unsigned long nbloc(m_Ppop->get_nbloc());
  unsigned long nploidie(m_Ppop->get_nploidie());
  unsigned long taille, i, j;
  Locus *Ploc;
  //	string nomloc;

  taille = nbloc * nploidie;

  while(m_tabPall.size() < taille)
    {
      m_tabPall.push_back(nullptr);
    }
  // cerr << "coucou 1" << endl;

  taille = individu.m_tabPall.size();
  for(i = 0; i < taille; i += nploidie)
    {
      Ploc = m_Ppop->_Pjeu->get_Plocus(individu.m_tabPall[i]->get_NomLocus());
      // cerr << "coucou 2" << Ploc->get_nom() << endl;
      for(j = 0; j < nploidie; j++)
        {
          ifPlacerAllele(Ploc,
                         ((Ploc->get_numloc() * nploidie) + j),
                         individu.m_tabPall[i + j]);
        }
    }
}


unsigned long
Individu::r_nballnonnuls(unsigned long locus, unsigned int nploidie) const
{
  // retourne le nb de copies d'allèles non nuls pour un locus
  unsigned long resultat(0);
  unsigned int j;

  for(j = 0; j < nploidie; j++)
    {
      if(m_tabPall[(locus * nploidie) + j]->_miss)
        continue;
      resultat++;
    }

  return (resultat);
}


const string
Individu::get_nom_all(unsigned long numlocus, unsigned long numall) const
{
  //	unsigned long nploidie(_Ppop->get_nploidie());
  unsigned long nbloc(m_Ppop->get_nbloc());

  if(numlocus >= nbloc)
    throw Anomalie(11);
  if(numall >= _nploidie)
    throw Anomalie(12);

  return (m_tabPall[(numlocus * _nploidie) + numall]->get_nom());
}


void
Individu::affiche(ostream &sortie, int choix) const
{
  unsigned long i, j;
  unsigned int nploidie(m_Ppop->get_nploidie());

  switch(choix)
    {
      case 1:
        // affichage d'un individu au format genepop
        sortie << _nom << " ,";

        for(i = 0; i < m_tabPall.size(); i += 2)
          {
            sortie << " " << m_tabPall[i]->get_nom()
                   << m_tabPall[i + 1]->get_nom();
          }
        break;

      case 2:
        // affichage d'un individu au format genepop en tenant compte des
        // introgressions
        sortie << _nom << " ,";

        for(i = 0; i < m_tabPall.size(); i += 2)
          {
            sortie << " ";
            if(m_Ppop->_tabAllIntrogressant.Existe(m_tabPall[i]))
              {
                sortie << "00";
              }
            else
              {
                sortie << m_tabPall[i]->get_nom();
              }
            if(m_Ppop->_tabAllIntrogressant.Existe(m_tabPall[i + 1]))
              {
                sortie << "00";
              }
            else
              {
                sortie << m_tabPall[i + 1]->get_nom();
              }
          }
        break;

      case 3:
        // affichage d'un individu au format populations en tenant compte des
        // introgressions
        sortie << _nom << " ,";

        for(i = 0; i < m_tabPall.size(); i += nploidie)
          {
            for(j = 0; j < nploidie; j++)
              {
                if(j == 0)
                  sortie << " ";
                else
                  sortie << ":";
                if(m_Ppop->_tabAllIntrogressant.Existe(m_tabPall[i + j]))
                  {
                    sortie << "*";
                  }
                if(m_tabPall[i + j]->r_estnul())
                  {
                    sortie << "00";
                  }
                else
                  {
                    sortie << m_tabPall[i + j]->get_nom();
                  }
                //			sortie << _tabPall[i+j]->_nom;
              }
          }
        break;
      case 4:
        // affichage d'un individu au format genetix
        sortie << _nom;
        for(i = 0; i < (10 - _nom.size()); i++)
          sortie << " ";

        for(i = 0; i < m_tabPall.size(); i += 2)
          {
            if((m_tabPall[i]->r_estnul()) || (m_tabPall[i + 1]->r_estnul()))
              sortie << " 000000";
            else
              {
                sortie << " ";
                if(m_tabPall[i]->_nom.size() == 2)
                  sortie << "0";
                else if(m_tabPall[i]->_nom.size() == 1)
                  sortie << "00";
                sortie << m_tabPall[i]->get_nom();
                if(m_tabPall[i + 1]->_nom.size() == 2)
                  sortie << "0";
                else if(m_tabPall[i + 1]->_nom.size() == 1)
                  sortie << "00";
                sortie << m_tabPall[i + 1]->get_nom();
              }
          }
        break;
      case 5:
        // affichage d'un individu au format Fstat

        for(i = 0; i < m_tabPall.size(); i += 2)
          {
            if((m_tabPall[i]->r_estnul()) || (m_tabPall[i + 1]->r_estnul()))
              sortie << "\t0";
            else
              sortie << "\t" << m_tabPall[i]->get_nom()
                     << m_tabPall[i + 1]->get_nom();
          }
        break;
      default:
        break;
    }
}


unsigned long
Individu::get_nballnuls(unsigned long locus) const
{
  // retourne le nombre d'alleles nuls de l'individu pour le locus
  //	long taille (_tabPall.size());
  unsigned long i;
  //	unsigned long nploidie(_Ppop->get_nploidie());
  unsigned long resultat(0);

  for(i = 0; i < _nploidie; i++)
    {
      if(m_tabPall[(locus * _nploidie) + i]->_miss)
        resultat++;
    }
  return (resultat);
}
/** Suppresssion d'un locus */
void
Individu::supprLocus(unsigned int numlocus)
{
  unsigned int nbloc(m_Ppop->get_nbloc());
  unsigned int i;

  if(numlocus >= nbloc)
    throw Anomalie(11);

  for(i = 0; i < _nploidie; i++)
    {
      m_tabPall.erase(m_tabPall.begin() + (numlocus * _nploidie));
    }
}

void
Individu::oPopulationsXML(unsigned int id,
                          ostream &sortie,
                          ostream &infos) const
{
  unsigned int i, j, nploidie(m_Ppop->get_nploidie()),
    nbloc(m_Ppop->get_nbloc());
  biolib::vecteurs::ChaineCar idXML;

  sortie << "<individual";
  idXML = m_Ppop->get_idXML();
  idXML += "i";
  idXML.AjEntier(id);
  // set_idXML(idXML);
  sortie << " id=\"" << idXML << "\"";
  sortie << " nploidy=\"" << nploidie << "\"";
  sortie << " name=\"" << get_nom() << "\"";
  sortie << ">" << endl;

  for(i = 0; i < nbloc; i++)
    {
      sortie << "<locus";
      sortie << " allid=\"";
      for(j = 0; j < nploidie; j++)
        {
          sortie << m_tabPall[(i * nploidie) + j]->get_idXML();
          if(j < (nploidie - 1))
            sortie << " ";
        }
      sortie << "\">";
      sortie << "</locus>" << endl;
    }

  sortie << "</individual>" << endl;
}

void
Individu::set_Pallele(Allele *Pall, unsigned int pos)
{
  unsigned int nploidie(get_nploidie());

  if(pos >= nploidie)
    throw Anomalie(12);

  pos = (Pall->get_Ploc()->get_numloc() * nploidie) + pos;

  m_tabPall[pos] = Pall;
}

bool
Individu::checkAlleleArraySize() const
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  if(m_Ppop == nullptr)
    {
      throw Anomalie(2);
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << m_Ppop;
  unsigned int nbloc(m_Ppop->get_nbloc());

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " nbloc=" << nbloc << " _nploidie=" << _nploidie;
  if(m_tabPall.size() != (nbloc * _nploidie))
    {
      throw Anomalie(14);
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return true;
}

const Allele *
Individu::getAllelePtr(std::size_t offset) const
{

  if(offset >= m_tabPall.size())
    {
      throw std::runtime_error("offset >= m_tabPall.size()");
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " _nom=" << _nom.c_str();

  if(!m_tabPall[offset])
    {
      throw std::runtime_error("!m_tabPall[offset]");
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(m_tabPall[offset] == 0)
    {
      throw std::runtime_error("m_tabPall[offset] == 0");
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  if(m_tabPall[offset] == NULL)
    {
      throw std::runtime_error("m_tabPall[offset] == NULL");
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  if(m_tabPall[offset] == nullptr)
    {
      throw std::runtime_error("m_tabPall[offset] == nullptr");
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " _tabPall[offset].get_Nom().c_str()=" << m_tabPall[offset]->_nom;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return m_tabPall[offset];
}

const vector<Allele *> &
Individu::get_tabPall() const
{
  return (m_tabPall);
}

unsigned long
Individu::r_nbcopall(Allele *Pall) const
{
  return (std::count(m_tabPall.begin(), m_tabPall.end(), Pall));
}


const Allele *
Individu::getAllelePtr(unsigned long numlocus, unsigned long numall) const
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << m_tabPall[(numlocus * _nploidie) + numall]->get_nom().c_str();
  return getAllelePtr((numlocus * _nploidie) + numall);
}


unsigned long
Individu::get_nballnuls() const
{
  // retourne le nombre d'alleles nuls de l'individu
  return std::count_if(m_tabPall.begin(),
                       m_tabPall.end(),
                       [](const Allele *allele) { return (allele->_miss); });
}
void
Individu::push_back_AllelePtr(Allele *Pall)
{
  m_tabPall.push_back(Pall);
}

bool
Individu::checkIndividu() const
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " " << _nom.c_str();
  checkAlleleArraySize();

  for(auto p_allele : m_tabPall)
    {
      if(p_allele == 0)
        {

          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          throw Anomalie(15);
        }
      if(p_allele == NULL)
        {

          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          throw Anomalie(15);
        }
      if(p_allele == nullptr)
        {

          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          throw Anomalie(15);
        }
      p_allele->checkAllele();
    }

  return true;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}
