/***************************************************************************
                          allele.h  -  description
                             -------------------
    begin                : Thu Sep 14 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#pragma once

//#include "arbres.h"
#include "vecteurs.h"

using namespace std;

class Individu;
class Population;
class Allele;
class StrucPop;
class Locus;
class Jeupop;
class ConstGnt;

typedef biolib::vecteurs::ChaineCar ChaineCar;
// allèle
class Allele
{
  public:
  //	Allele();
  Allele(Locus *);
  //	Allele(Locus*, const Allele &);
  Allele(const Allele &, Locus *Ploc); // constructeur de sopies
  Allele(Locus *, const char *);
  Allele(Locus *, const string &);
  ~Allele();
  bool f_verifnum(int) const;

  bool checkAllele() const;
  Locus *
  get_Ploc() const
  {
    return (_Ploc);
  };
  const string
  get_nom() const
  {
    return (_nom.toStdString());
  };
  const string get_NomLocus() const;
  const unsigned int
  get_nbrepet() const
  {
    return (_nbrepet);
  };
  void
  set_nbrepet(unsigned int nbrepet)
  {
    _nbrepet = nbrepet;
  };

  void
  set_Plocus(Locus *Ploc)
  {
    _Ploc = Ploc;
  };
  void
  set_nom(const char *mot)
  {
    _nom = mot;
  };
  void
  set_nom(const QString &mot)
  {
    _nom = mot;
  };
  void
  set_idXML(const string &id)
  {
    _idXML = id;
  };

  const string &
  get_idXML() const
  {
    return (_idXML);
  };

  bool
  r_estnul() const
  {
    return (_miss);
  };
  bool
  r_nonnul() const
  {
    return (!_miss);
  };

  const Allele &operator=(const Allele &);
  bool operator==(const Allele &rval) const;

  friend class Locus;
  friend class Individu;
  friend class StrucPop;
  friend class Jeupop;
  friend class DistancesGnt;

  private:
  //	int _occurence; //nb d'occurences de cet allèle dans le jeu de populations
  bool r_ismissingvalue(const QString &) const;
  QString _nom;
  //	char _nomgpop[4];
  Locus *_Ploc;
  bool _miss; // missing value
  unsigned int
    _nbrepet; // specifique aux microsatellite: nombre de repetitions de motifs
  //	int _nbind; //nb d'individus pourvus de cet allèle
  //	Individu ** _tabPind;
  string _idXML; // sert à la lecture et à l'écriture en XML
};

