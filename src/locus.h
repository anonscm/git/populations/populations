/***************************************************************************
                          locus.h  -  Librairie d'objets permettant de manipuler
 des données spécifiques aux locus
                             -------------------
    begin                : ven sep 01 10:25:55 CEST 2000
    copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// bibliothèque d'objets permettant de manipuler des
// donnees sur des populations
// ecrit par Olivier Langella le 21/4/98
// modifie le 01/09/2000 par Olivier Langella pour apporter les capacités de
// structurations de populations


#pragma once

class Jeupop;
#include "allele.h"
#include "vecteurs.h"
// typedef biolib::vecteurs::ChaineCar ChaineCar;


long double fact(int n);
long double fact(int n, int m);
long double comb(int n, int m);

// locus
class Locus
{
  public:
  //	Locus(const Locus&); //constructeur de copies
  Locus(const Locus &original, Jeupop *Pjeu); // constructeur de copies
  Locus(long nball);
  Locus(Jeupop *, long nball);
  Locus(const Locus &other);
  ~Locus();

  bool checkLocus() const;

  void reset(Jeupop *, long); // pointeur sur Jeupop,nball
  Allele *new_allele(const string &name, unsigned int nbrepeat);


  //	int getnballdif() const;
  unsigned long
  get_nball() const
  {
    return (m_tabPall.size());
  };
  unsigned long get_numloc();
  Allele *getPall(const string &nom) const;
  Allele *getPallNul();
  Allele *getPall(std::size_t i) const;
  const char *
  get_nomall(int i) const
  {
    return (m_tabPall[i]->get_nom().c_str());
  };
  //	const char * get_nom() {return(_nom.c_str());};
  const string get_nom() const;
  bool f_verifnum(int) const;

  inline void set_PJeupop(Jeupop *Pjeu);
  void set_nom(const string &mot);
  void set_microsat_correction(long double valmin,
                               long double valmax,
                               unsigned int value);

  void ifAjouterAllele(const Allele *Pall);

  void push_back_Allele(Allele *Pall);


  const Locus &operator=(const Locus &);
  /** Enlève les allèles non représentés dans Jeupop
   */
  void f_nettoieAlleles();
  /** Retourne le nombre d'allèles non nuls pour ce locus */
  unsigned long get_nballnonnuls() const;

  void f_trad2Gpop(ostream &) const;

  private:
  QString _nom;
  Jeupop *_Pjeu;
  vector<Allele *> m_tabPall;
};
