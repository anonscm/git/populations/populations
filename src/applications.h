/***************************************************************************
                          applications.h  -  Librairie d'objets pour creer des applications
                             -------------------
    begin                : ven aug 14 10:25:55 CEST 2000
    copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef APPLICATIONS_H
#define APPLICATIONS_H

//#define __ENABLE_WSTRING
#include <QStringList>

#include "internat.h"

#include "matrices.h"

# ifndef COMPILATION_MINGW32
//# include "gzstream.h"
# endif

using namespace std;
using namespace biolib::vecteurs;

class Application {
public:

	Application();
	virtual ~Application() {};

	virtual void lancement(QStringList arguments);
  /** test si nomfichier existe */
  virtual bool fFichierExiste(const string & nomfichier);
	
protected:
	virtual int menu_principal();
	virtual int menu_formatMatrice();
	virtual void affPubEntree() const;
	virtual void affPubSortie() const;
	virtual void fLigneCommande(QStringList arguments);

	virtual bool DemandeOuiNon(char laquestion[]) const;
	virtual int DemandeEntier(const string & laquestion, int inf, int sup) const;
	virtual int DemandeEntier(char laquestion[], int inf, int sup, int defaut) const;
	virtual float DemandeReel(char laquestion[], float inf, float sup) const;
	virtual int DemandeChoix(int inf, int sup) const;
	virtual string DemandeString(char laquestion[]) const;
	virtual string DemandeFichier(const string & laquestion);
	bool litMatriceLD(MatriceLD & mat);
	bool litJeuMatriceLD(JeuMatriceLD &jeumat);
	bool ecritMatriceLD(MatriceLD & mat, string nomFichier="", int format=0);
	bool ecritJeuMatriceLD(const JeuMatriceLD & jeumat, string nomFichier="", int format=0);


	string _nomFichier;
	int _choix;
	int _formatMatrice; // 1-> Excel


	ifstream _fichier;

	ofstream _sortie;

	bool _confirmeEcraseFichier;

};

#endif

