
# this command finds Qt4 libraries and sets all required variables
# note that it's Qt4, not QT4 or qt4
#/usr/share/cmake-2.6/Modules/FindQt4.cmake
SET (QT_USE_QTXML true)
FIND_PACKAGE( Qt5 COMPONENTS Core REQUIRED )


#FIND_PACKAGE(X11 REQUIRED)
#FIND_PACKAGE(FindThreads REQUIRED)

IF(WIN32)
  ADD_DEFINITIONS(-DMINGW32)
ENDIF(WIN32)

#INCLUDE_DIRECTORIES(X11_INCLUDE_DIR)
# X11_FOUND is true if X11 is available.
#    * X11_INCLUDE_DIR contains the include directories to use X11.
#    * X11_LIBRARIES points to the libraries to link against to use X11.
# Make sure the compiler can find include files from our Hello library.
#include_directories (${X11_INCLUDE_DIR})

# -m -pthread FindThreads


# Make sure the linker can find the Hello library once it is built.
#link_directories (${X11_INCLUDE_DIR})

add_executable (populations populations.cpp distgnt.cpp jeupopexp.cpp fstat.cpp jeupop.cpp applpop.cpp applpopulations.cpp matrices.cpp applications.cpp 
arbre.cpp arbreplus.cpp couleur.cpp vecteurs.cpp locus.cpp allele.cpp individu.cpp strucpop.cpp chaineficpop.cpp metapop.cpp population.cpp) 


IF(WIN32)
	target_link_libraries (populations Qt5::Core)
ELSE(WIN32)
	target_link_libraries (populations Qt5::Core m)
ENDIF(WIN32)

 
