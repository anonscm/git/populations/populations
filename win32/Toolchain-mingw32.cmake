# cd buildwin32
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win32/Toolchain-mingw32.cmake ..
# scp index.html tryphon@bioinformatics.org:~/public_html/populations
#
SET (MINGW32 1)

# this one is important
SET(CMAKE_SYSTEM_NAME Windows)
#this one not so much
SET(CMAKE_SYSTEM_VERSION 1)

SET (WINDAUBE_ENV_PATH /opt/win32)
# it worked on 27 nov 2013 :
# download and install from http://download.qt-project.org/archive/qt/4.8/4.8.2/ the mingw32 version

SET(QT_PREFIX    ${WINDAUBE_ENV_PATH}/Qt)

# specify the cross compiler
SET(CMAKE_C_COMPILER   /usr/bin/i586-mingw32msvc-gcc)
SET(CMAKE_CXX_COMPILER /usr/bin/i586-mingw32msvc-g++)
SET(CMAKE_RC_COMPILER /usr/bin/i586-mingw32msvc-windres)

# where is the target environment
#http://qt.nokia.com/downloads/windows-cpp
SET(CMAKE_FIND_ROOT_PATH  /usr/i586-mingw32msvc ${QT_PREFIX})

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)


set(Qwt5_INCLUDE_DIR ${QWT_PREFIX}/src)
set(Qwt5_Qt4_LIBRARY ${QWT_PREFIX}/lib)
SET( Qwt5_Qt4_FOUND TRUE )
SET( Qwt5_Qt4_TENTATIVE_LIBRARY ${Qwt5_Qt4_LIBRARY}/libqwt5.a )

set(QT_HEADERS_DIR ${QT_PREFIX}/include)
set(QT_LIBRARY_DIR ${QT_PREFIX}/lib)

set(QT_QTCORE_LIBRARY ${QT_PREFIX}/lib/libQtCore4.a)
set(QT_QTCORE_LIBRARY_RELEASE ${QT_PREFIX}/lib/libQtCore4.a)
set(QT_QTCORE_INCLUDE_DIR ${QT_PREFIX}/include/QtCore)

set(QT_QTXML_LIBRARY ${QT_PREFIX}/lib/libQtXml4.a)
set(QT_QTXML_LIBRARY_RELEASE ${QT_PREFIX}/lib/libQtXml4.a)
set(QT_QTXML_INCLUDE_DIR ${QT_PREFIX}/include/QtXml)

set(QT_QTGUI_LIBRARY ${QT_PREFIX}/lib/libQtGui4.a)
set(QT_QTGUI_LIBRARY_RELEASE ${QT_PREFIX}/lib/libQtGui4.a)
set(QT_QTGUI_INCLUDE_DIR ${QT_PREFIX}/include/QtGui)

set(QT_QTXMLPATTERNS_LIBRARY ${QT_PREFIX}/lib/libQtXmlPatterns4.a)
set(QT_QTXMLPATTERNS_RELEASE ${QT_PREFIX}/lib/libQtXmlPatterns4.a)
set(QT_QTXMLPATTERNS_INCLUDE_DIR ${QT_PREFIX}/include/QtXmlPatterns)


#set(QT_BINARY_DIR   ${QT_PREFIX}/bin)
#set(QT_LIBRARY_DIR  ${QT_PREFIX}/lib)
#set(QT_QTCORE_LIBRARY   ${QT_PREFIX}/lib/libQtCore4.a)
#set(QT_QTCORE_INCLUDE_DIR ${QT_PREFIX}/include/QtCore)
#set(QT_MOC_EXECUTABLE  ${QT_PREFIX}/moc)
#set(QT_QMAKE_EXECUTABLE  ${QT_PREFIX}/qmake)
#set(QT_UIC_EXECUTABLE  ${QT_PREFIX}/uic)
